import expect from 'expect.js'
import guessReducer from './../app/reducers/guessReducer.js'
import configReducer from './../app/reducers/configReducer.js'

describe("guess activity", function () {
  it('updates lastGuess in redux state', function () {
    var stateBefore;
    var action = {
      type: "NEW_GUESS",
      guess: "5"
    };
    var stateAfter = {
      lastGuess: "5"
    }
    
    expect(
     guessReducer(stateBefore, action)
    ).to.eql(stateAfter);
  });

  it('doesn`t update state with a non-number guess', function () {
    var stateBefore = {
      lastGuess: "8"
    };
    var action = {
      type: "NEW_GUESS",
      guess: "string"
    };
    var stateAfter = {
      lastGuess: "8"
    }
    
    expect(
     guessReducer(stateBefore, action)
    ).to.eql(stateAfter);
  });
});

describe("config activity", function () {
  context("resetting the game", function () {
    it('applies new bounds to state', function () {
      var stateBefore;
      var action = {
        type: "RESET_GAME",
        upperBound: 2,
        lowerBound: 4
      };
      var stateAfter = {
        upperBound: 2,
        lowerBound: 4
      }

      expect(
       configReducer(stateBefore, action).upperBound
      ).to.be(stateAfter.upperBound);
      expect(
       configReducer(stateBefore, action).lowerBound
      ).to.be(stateAfter.lowerBound);
    });

    it('resets lastGuess to empty string', function () {
      var stateBefore = {
        lastGuess: "5"
      };
      var action = {
        type: "RESET_GAME"
      };
      var stateAfter = {
        lastGuess: ""
      };
      
      expect(
       guessReducer(stateBefore, action)
      ).to.eql(stateAfter);
    })

    it('doesn`t update state with a non-number bounds', function () {
      var stateBefore = {
        upperBound: 2,
        lowerBound: 4
      }
      var action = {
        type: "NEW_GUESS",
        upperBound: "",
        lowerBound: [1,2,3]
      };

      var stateAfter = {
        upperBound: 2,
        lowerBound: 4
      }
      
      expect(
       configReducer(stateBefore, action).upperBound
      ).to.be(stateAfter.upperBound);
      expect(
       configReducer(stateBefore, action).lowerBound
      ).to.be(stateAfter.lowerBound);
    });
  })
});