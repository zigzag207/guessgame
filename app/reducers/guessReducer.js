const defGuessState = {
  lastGuess: ""
}

const guessReducer = (
  state = defGuessState,
  action
) => {
  switch (action.type) {
    case "NEW_GUESS":
      if (isNaN(action.guess)) {
        return state;
      }

      return {
        lastGuess: action.guess
      };
    case "RESET_GAME":
      return {
        lastGuess: ""
      };
    default:
      return state;
  }
}

export default guessReducer