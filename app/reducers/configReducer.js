import Util from './../util.js'

const defConfigState = {
  hiddenNum: Util.randomIntWithBounds(1, 10),
  lowerBound: 1,
  upperBound: 10
}

const configReducer = (
  state = defConfigState,
  action
) => {
  switch (action.type) {
    case "RESET_GAME":
      let low = parseInt(action.lowerBound);
      let upp = parseInt(action.upperBound);

      if (isNaN(low) || isNaN(upp)) {
        return state;
      }

      return {
        upperBound: upp,
        lowerBound: low,
        hiddenNum: Util.randomIntWithBounds(low, upp)
      };
    default:
      return state;
  }
}

export default configReducer