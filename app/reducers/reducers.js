import { combineReducers } from 'redux'
import guessReducer from './guessReducer'
import configReducer from './configReducer'

export default combineReducers({
  guessReducer,
  configReducer
});