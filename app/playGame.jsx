import React from 'react'
import Util from './util'
import { connect } from 'react-redux'


let Play = ({
  lastGuess,
  hiddenNum,
  submitGuess,
  lowerBound,
  upperBound
}) => {
  const lg = parseInt(lastGuess);
  const hn = parseInt(hiddenNum);
  let status;
  if (lastGuess === "") {
    lastGuess = "none";
    status = "";
  } else {
    if (lg < hn) {
      status = "Nope. Higher.";
    } else if (lg > hn) {
      status = "Nope. Lower."
    } else {
      status = "You got it!"
    }
  }


  let guess;
  return (
    <div className="column control is-half is-offset-one-quarter">
      <h1>Play!</h1>
      <h3> Guess the number between {lowerBound} and {upperBound} </h3>
      <p>Last guess: {lastGuess}</p>
      <p> {status} </p>
      <label htmlFor='guess' className="label">Guess: </label>
      <input
        type="number"
        name="guess"
        className='input'
        ref={(node)=> {
          guess=node;
        }} ></input>
      <a onClick={(e) => {
          e.preventDefault();
          submitGuess(guess.value);
        }}
        className="button is-primary">Make Guess</a>
    </div>
  );
}

const mapStateToPlayProps = (
  state
) => {
  return {
    lastGuess: state.guessReducer.lastGuess,
    hiddenNum: state.configReducer.hiddenNum,
    lowerBound: state.configReducer.lowerBound,
    upperBound: state.configReducer.upperBound
  }
}

const mapDispatchToPlayProps = (
  dispatch
) => {
  return {
    submitGuess: (num) => {
      dispatch({
        type: "NEW_GUESS",
        guess: num
      });
    }
  }
}

export default connect(
  mapStateToPlayProps,
  mapDispatchToPlayProps
)(Play);