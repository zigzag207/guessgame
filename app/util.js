
var Util = {
   randomIntWithBounds: function (lower, upper) {
     var range = upper - lower;
     return Math.trunc((Math.random() * range) + lower);
   }
}

export default Util;
