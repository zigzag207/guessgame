import { connect } from 'react-redux';
import Util from './util'
import React from 'react'


let Config = ({
  upperBound,
  lowerBound,
  submitReset
}) => {
  let lbv;
  let ubv;

  return (
    <div className="column control is-half is-offset-one-quarter">
      <h3>Game Config</h3>
      <label htmlFor='lower' className="label">Lower bound: </label>
      <input
        type='text'
        name='lower'
        className='input'
        ref={node => {
          lbv=node;
        }}
        placeholder={lowerBound}>
        </input>
      <label htmlFor='upper' className="label">Upper bound: </label>
      <input
        type='text'
        name='upper'
        className='input'
        ref={node => {
          ubv=node;
        }}
        placeholder={upperBound}>
        </input>
      <a onClick={(e) => {
        if (!ubv.value) {
          ubv.value = 10;
        }
        if (!lbv.value) {
          lbv.value = 1;
        }
        e.preventDefault
        submitReset(ubv.value, lbv.value);
      }} className="button is-primary">Reset</a>
    </div>
  );
}

const mapStateToConfigProps = (state) => {
  return {
    upperBound: state.configReducer.upperBound,
    lowerBound: state.configReducer.lowerBound
  }
}

const mapDispatchToConfigProps = (dispatch) => {
  return {
    submitReset: (upperBound, lowerBound) => {
      dispatch({
        type: "RESET_GAME",
        upperBound,
        lowerBound
      })
    }
  }
}

export default connect(
  mapStateToConfigProps,
  mapDispatchToConfigProps
)(Config);