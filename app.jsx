import Util from './app/util'
import { connect } from 'react-redux';
import guessApp from './app/reducers/reducers'
import PlayComponent from './app/playGame'
import GameConfig from './app/configGame'
import { render } from 'react-dom'
import React from 'react'

const GuessApp = () => (
  <div className="content box">
    <PlayComponent />
    <GameConfig />
  </div>
)

import { Provider } from 'react-redux';
// debugger
import { createStore } from 'redux';

render(
  <Provider store={createStore(guessApp)}>
    <GuessApp />
  </Provider>,
  document.getElementById('root')
);