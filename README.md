#Guess The Number

A refactored JavaScript app with React/Redux, node, Bulma CSS, expect.js and Mocha for testing. This app assumes you have Node, npm previously installed. If not, your first step should be following the instructions here:
https://nodejs.org/en/download/package-manager/
and here:
https://docs.npmjs.com/getting-started/installing-node

##Instructions

* Clone this repo
* Navigate into the right directory in the terminal

   `cd guessgame`

* In the terminal run:

   `npm install`

   `webpack`

* Now you can open the index.html file in your browser

* To run the automated tests, in the terminal run
   
   `./node_modules/.bin/mocha --compilers js:babel-register`

<!-- My Notes -->
<!--  -->


So I've refactored this into a small React/Redux app.

It has two components now: GuessPlay and ConfigPlay, and their associated reducers.

Redux is a fantastic way to simplify the store/state aspect of React, especially for simple web UI.

Normally, there would also be 'actions' and 'constants' to help facilitate the component-to-store relationship in a react/redux app, but the scale of this app did not really call for it.

Were I to continue to work on this app, building action creators would be my very next step, as they are crucial to keeping a React app organized. This would also make the app far more testable. (Breaking things into more parts is always good for testing.)

I used Bulma CSS to style it up a bit, although this was not a focus of my work.

I used expect and Mocha for testing. I've really only built testing for the reducers. I thought about using Capybara and making some unit tests for the markup, but decided not to.



## Original instructions

At Vistar, we like to play "Guess the Number" in our downtime. One of our
engineers hacked together our current version using raw jQuery. We're
trying to modernize with some new technology.

Some improvements we'd like for the new version:

* Replace jQuery with a modern framework
* Separate code and markup
* Break up UI into modular components
* Automated tests

Our current version looks plain. You may want to spruce it up,
but we won't dock you if you don't.

## Notes

Feel free to take whatever liberties with the implementation you like. No need
to go overboard - if you spend more than two hours on it, maybe take a step
back.

And, if there's anything special we have to do to run your program, just let us
know. A Makefile never hurt anyone.

